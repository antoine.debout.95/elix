﻿ELIX - LOGICIEL DE RENOMMAGE DE FICHIERS DANS DES REPERTOIRES

Auteur : Antoine DEBOUT
Date de création : 24/11/2017
Langage : Python
Git : https://gitlab.com/antoine.debout.95/elix.git

Librairies utilisées : 
    - sys 
    - tkinter 
    - tkinter.messagebox 
    - unittest 
    - os 
    - glob 
    - join 
    - json

Fichier du menu principal : Elix\vue\vueInterface.py 

Classes du projet:
    - Classes d'interface:
        * modele/Interface
        * modele/Apropos
        * modele/Creer
        * modele/Lister
        * modele/Simulation
    - Classes Modeles:
        * modele/ListeRegle
        * modele/Action
        * modele/Regle
        * modele/Renommage
    - Classes de Test Unitaires:
        * unittest/TestAction
        * unittest/TestRegle
        * unittest/TestListeRegle
        * unittest/TestLister

Fonctionnalités Version 1.0 :
    - Ouverture de l'application sur une liste de règles. Sélectionner une règle
    - L'interface de renommage s'ouvre avec le récapitulatif des attributs de la règle
    - Choisir le répertoire contenant les fichiers à renommer avec la boite de dialogue
    - Cliquer sur le bouton renommer pour afficher la simulation et valider pour renommer les fichiers
    - Affichage du A propos sur l'onglet ? -> A propos
    - Appel de la fenêtre de création sur l'onglet Regle -> Créer
    - Pour changer de règle et ouvrir l'interface de listage des Règle : onglet Regle -> Lister

Gestion d'erreurs:
    - Interface Lister:
        * Gestion d'un Header sur lequel on ne peut pas cliquer
    - Interface Créer:
        * Si l'amorce est "Chiffre", on ne peut saisir que des chiffre + vérification qu'il est bien inférieur à 999
        * Si l'amorce est "Lettre", on ne peut saisir que des lettre + vérification qu'elles sont inférieur à ZZZ
    - Interface Interface:
        * Si l'amorce est "Chiffre", on ne peut saisir que des chiffre + vérification qu'il est bien inférieur à 999
        * Si l'amorce est "Lettre", on ne peut saisir que des lettre + vérification qu'elles sont inférieur à ZZZ
        * Si il n'y à pas de répertoire de saisi, on obtient un messagebox d'avertissement
        * Si une extension indiquée n'est pas comprise dans le répertoire, on obtient un messagebox d'avertissement
        * Si il n'y a pas de fichier dans le répertoire, on obtient un messagebox d'avertissement

Création du .exe:
L'executable à été créer avec pyinstaller