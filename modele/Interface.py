import sys
sys.dont_write_bytecode = True
from tkinter import *
from tkinter.messagebox import *
from Apropos import *
from Regle import *
from Action import *
from Creer  import *
from Renommage import *
from Lister import *
from Simulation import *
import tkinter.filedialog as fdtk


"""
Classe d'affichage de la fenetre principale
    
Auteur: Antoine DEBOUT
Date de creation: 28/11/2017
Version: 1.0
"""
class Interface:
    """
        Constructeur de la classe
        @param Null
    """
    def __init__(self, am="Aucun", ap="", pr="", nomf=True, pos="", ext="",nr=""):
        """
        Ajout des attributs de la classe
        """
        self.amorceTxt = am
        self.apartirdeTxt = ap
        self.prefixeTxt = pr
        self.nomFichierTxt = nomf
        self.postfixeTxt = pos
        self.extensionsTxt = ext
        self.nomRepertoire = nr
        """
        Définition de la fenêtre
        """
        self.root = Tk()
        self.root.title("Projet Elix - Renommage de fichier")
        self.fenrw = 950
        self.fenrh = 600
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        self.root.geometry("%dx%d+%d+%d" % (self.fenrw, self.fenrh, (self.sw-self.fenrw)/2, (self.sh-self.fenrh)/2))
        self.root.resizable(width=False, height=False)

        """
        Définition du Menu Bar
        """
        self.menubar = Menu(self.root)

        """
        Définition de l'onglet Regle du Menu Bar
        """
        self.menuRegle = Menu(self.menubar, tearoff=0)
        self.menuRegle.add_command(label="Lister", command=self.shLister)
        self.menuRegle.add_command(label="Créer", command=self.shCreer)
        self.menuRegle.add_separator()
        self.menuRegle.add_command(label="Quitter", command=self.root.destroy)
        self.menubar.add_cascade(label="Fichier", menu=self.menuRegle)

        """
        Définition de l'onglet ? du Menu Bar
        """
        self.menuAide = Menu(self.menubar, tearoff=0)
        self.menuAide.add_command(label="A propos", command=self.shApropos)
        self.menubar.add_cascade(label="?", menu=self.menuAide)

        """
        Configuration de la fenêtre
        """
        self.root.config(menu=self.menubar)

        """
        Définition de la photo de projet
        """
        #Source de la photo
        self.photo = PhotoImage(file="../img/tyrion.png")
        #Canvas pour le positionnement de la photo
        self.canvasPhoto = Canvas(self.root,width=272, height=204)
        self.canvasPhoto.create_image(0,0,anchor=NW,image=self.photo)
        self.canvasPhoto.pack(side="right",fill="both", padx=5,pady=5)

        """
        Définition du Label Titre Principal
        """
        self.labelMain = Label(self.root, text="Renommage en Lots", font=("Helvetica", 16))
        self.labelMain.pack()

        """
        Définition Frame Champs Nom Répertoire
        """
        self.frameNomRepertoire = Frame(self.root,borderwidth=0, relief=GROOVE)
        self.frameNomRepertoire.place(x=50, y=200)
        self.labelNomRepertoire = Label(self.frameNomRepertoire,text="Nom du répertoire :")
        self.labelNomRepertoire.pack(side="left", fill="both")
        self.btnNR = Button(self.frameNomRepertoire, text="...", width=3, command=self.onClickBoutonRepertoire)
        self.btnNR.pack(side="right")
        self.stringNr = StringVar()
        self.stringNr.set(self.nomRepertoire)
        self.saisieNomRepertoire = Entry(self.frameNomRepertoire, width=60, textvariable=self.stringNr)
        self.saisieNomRepertoire.pack(side="right", fill="both",padx=3, pady=3)
        
        """
        Définition Frame Liste déroulante Amorce
        """
        self.frameAmorce = Frame(self.root,borderwidth=0, relief=GROOVE)
        self.frameAmorce.place(x=50, y=300)
        self.labelAmorce = Label(self.frameAmorce, text="Amorce")
        self.labelAmorce.pack(side="top", fill="both")
        self.optionListeAmorce = ("Aucun","Chiffre","Lettre")
        self.stringAmort = StringVar()
        self.stringAmort.set(self.amorceTxt)
        self.listMenuAmorce = OptionMenu(self.frameAmorce, self.stringAmort, *self.optionListeAmorce)
        self.listMenuAmorce.pack(side="bottom", fill="both")

        """
        Définition Frame Champ Préfix
        """
        self.framePrefixe = Frame(self.root, borderwidth=0,relief=GROOVE)
        self.framePrefixe.place(x=150, y=300)
        self.labelPrefixe = Label(self.framePrefixe, text="Préfixe")
        self.labelPrefixe.pack(side="top", fill="both")
        self.stringPrefixe = StringVar()
        self.stringPrefixe.set(self.prefixeTxt)
        self.saisiePrefixe = Entry(self.framePrefixe, width=20, textvariable=self.stringPrefixe)
        self.saisiePrefixe.pack(side="bottom", fill="both")

        """
        Définition Frame Radio Bouton
        """
        self.frameRadioNom = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameRadioNom.place(x=280, y=300)
        self.labelRadioNom = Label(self.frameRadioNom, text="Nom du fichier")
        self.labelRadioNom.pack(side="top", fill="both")
        self.valeurRadioNom = StringVar()
        self.valeurRadioOtherNom = StringVar()
        if(self.nomFichierTxt != True):
            self.valeurRadioOtherNom.set(self.nomFichierTxt)
        self.choix1 = Radiobutton(self.frameRadioNom, text="Nom Original", variable=self.valeurRadioNom, value=1)
        self.choix1.pack()
        self.frameRadioEntry= Frame(self.frameRadioNom, borderwidth=0, relief=GROOVE)
        self.frameRadioEntry.pack(side=LEFT, padx=5,pady=5)
        self.choix2 = Radiobutton(self.frameRadioEntry, variable=self.valeurRadioNom, value=2)
        self.entryChoix = Entry(self.frameRadioEntry, textvariable=self.valeurRadioOtherNom)
        self.choix2.pack(side=LEFT)
        if(self.nomFichierTxt == True):
            self.choix1.select()
        else:
            self.choix2.select()
        self.entryChoix.pack(side=RIGHT)

        """
        Définition Frame Champ Postfixe
        """
        self.framePostfixe = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.framePostfixe.place(x=480,y=300)
        self.labelPostfixe = Label(self.framePostfixe, text="Postfixe")
        self.labelPostfixe.pack(side="top", fill="both")
        self.stringPostfixe = StringVar()
        self.stringPostfixe.set(self.postfixeTxt)
        self.saisiePostfixe = Entry(self.framePostfixe, width=20, textvariable=self.stringPostfixe)
        self.saisiePostfixe.pack(side="bottom", fill="both")

        """
        Définition Frame Champs Extension
        """
        self.frameExtension = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameExtension.place(x=630, y=300)
        self.labelExtension = Label(self.frameExtension, text="Extensions Concernées (.jpg,.txt...)(séparer par des virgules)")
        self.labelExtension.pack(side="top", fill="both")
        self.stringExtensions = StringVar()
        self.stringExtensions.set(self.extensionsTxt)
        self.saisieExtension = Entry(self.frameExtension, width=20, textvariable=self.stringExtensions)
        self.saisieExtension.pack(side="bottom", fill="both")

        """
        Définition Frame Champ A parir de
        """
        self.frameApartirde = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameApartirde.place(x=50, y=500)
        self.labelApartirde = Label(self.frameApartirde, text="A partir de")
        self.labelApartirde.pack(side="top", fill="both")
        self.stringApartirde = StringVar()
        self.stringApartirde.set(self.apartirdeTxt)
        self.saisieApartirde = Entry(self.frameApartirde, width=20, textvariable=self.stringApartirde)
        self.saisieApartirde.pack(side="bottom",fill="both")

        """
        Définition du bouton
        """
        self.boutonRenommer = Button(self.root, text="Renommer", width=20, command=self.clickBoutonRename)
        self.boutonRenommer.place(x=630, y=500)

        """
        MainLoop de la fenêtre
        """
        self.root.mainloop()

        
    """
    Fonction d'affichage d'une alerte de test
    @param NULL

    @return True
    """
    def alert(self):
        showinfo("Test","Info")
        return True

    """
    Fonction d'appel de l'Interface A propos
    @param NULL

    @return True
    """
    def shApropos(self):
        Apropos()
        return True

    """
    Fonction d'appel de l'Interface Creer
    @param NULL

    @return True
    """
    def shCreer(self):
        self.root.destroy()
        Creer()
        return True
    
    """
    Fonction d'appel de l'Interface Lister

    @return True
    """
    def shLister(self):
        self.root.destroy()
        Lister()

        return True
    
    """
    Méthode de séléction d'un répertoire pour simplifier le champ Nom du répertoire

    @return True
    """
    def onClickBoutonRepertoire(self):
        directory = fdtk.askdirectory()
        self.stringNr.set(directory)
        return True
        
    """
    Méthode appelé lors du cli sur le bouton de renommage
    Appel de la classe Regle
    Appel de la classe Renommage

    @return Bool
    """
    def clickBoutonRename(self):
        repertoire = self.saisieNomRepertoire.get()
        prefixe = self.saisiePrefixe.get()
        postfixe = self.saisiePostfixe.get()
        amorce = self.stringAmort.get()
        apartirde = self.saisieApartirde.get()
        if len(apartirde) <= 3 and ((amorce == "Chiffre" and (apartirde.isnumeric() or apartirde =="")) or (amorce=="Lettre" and (apartirde.isalpha() or apartirde ==""))) or amorce=="Aucun":
            if amorce == "Chiffre" and apartirde =="":
                apartirde = 1
            if amorce == "Lettre" and apartirde =="":
                apartirde = "A"
            if self.valeurRadioNom.get() == "1":
                nomFichier = True
            if self.valeurRadioNom.get() == "2":
                nomFichier = self.valeurRadioOtherNom.get()
            extensions = self.saisieExtension.get().split(",")
            #Instanciation d'une Règle
            regle = Regle(amorce,apartirde,prefixe,nomFichier,postfixe)
            regle.extensions[:] = []
            for i in extensions:
                regle.ajoutExtension("*"+i)
            extensions = str(regle.extensions)
            if(repertoire != ""):
                a = Action(repertoire,regle)
                simulation = a.simule()
                message = simulation[0]
                dict = simulation[1]
                if message != "Erreur, une ou plusieurs extensions saisies ne sont pas dans le répertoire":
                    self.root.destroy()
                    simulInterface = Simulation(message,dict,repertoire,regle,extensions) 
                else:
                    messagebox.showerror("Erreur",message)
            else:
                showinfo("Simulation", "Il n'y a pas de répértoire de saisi")
            return True
        else:
            showerror("Erreur","L'amorce et/ou A partir de ne sont pas cohérents.")
            return False 
        

