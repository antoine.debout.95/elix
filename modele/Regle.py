import sys
sys.dont_write_bytecode = True

"""
Classe Modele des Regles de l'application

Auteur: Antoine DEBOUT
Date de création: 28/11/2017
Version: 1.0
"""
class Regle:
    """
    Constructeur de la classe Regle
    @param String amorce
    @param String apartirde
    @param String prefixe
    @param String nomfichier
    @param String postfixe
    @param List extensions
    """
    def __init__(self, amorce,apartirde,prefixe,nomfichier,postfixe,extensions = []):
        self.amorce = amorce
        self.apartirde= apartirde
        self.prefixe = prefixe
        self.nomfichier = nomfichier
        self.postfixe = postfixe
        self.extensions = extensions

    """
    Méthode toString de la classe Regle

    @return String
    """
    def __str__(self):
        return "Renomage du fichier en..."

    """
    Getter de l'attribut amorce
    @return String
    """    
    def _get_amorce(self):
        return self.amorce
    
    """
    Setter de l'attribut amorce 
    @param String a

    @return True
    @return String
    """
    def _set_amorce(self, a):
        try:
            self.amorce = a
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : String attendu"
    
    """
    Getter de l'attribut apartirde
    @return String
    """  
    def _get_apartirde(self):
        return self.apartirde

    """
    Setter de l'attribut apartirde 
    @param String a

    @return True
    @return String
    """
    def _set_apartirde(self, a):
        try:
            self.apartirde = a
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : String attendu"

    """
    Getter de l'attribut prefix
    @return String
    """         
    def _get_prefix(self):
        return self.prefixe

    """
    Setter de l'attribut prefix 
    @param String p

    @return True
    @return String
    """
    def _set_prefix(self, p):
        try:
            self.prefixe = p
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : String attendu"
    
    """
    Getter de l'attribut nomfichier
    @return String
    """  
    def _get_nomFichier(self):
        return self.nomfichier

    """
    Setter de l'attribut nomfichier 
    @param String n

    @return True
    @return String
    """
    def _set_nomFichier(self,n):
        try:
            self.nomfichier = n
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : String attendu"

    """
    Getter de l'attribut postfixe
    @return String
    """
    def _get_postfixe(self):
        return self.postfixe
    
    """
    Setter de l'attribut postfixe 
    @param String p

    @return True
    @return String
    """
    def _set_postfixe(self,p):
        try:
            self.postfixe = p
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : String attendu"

    """
    Getter de l'attribut extensions
    @return List
    """  
    def _get_extensions(self):
        return self.extensions
    
    """
    Setter de l'attribut extensions 
    @param String e

    @return True
    @return String
    """
    def _set_extensions(self,e):
        try:
            self.extensions = e
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : List attendu"

    """
    Ajout des extensions

    @return True
    """
    def ajoutExtension(self,ext):
        self.extensions.append(ext)
        return True