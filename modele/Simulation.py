import sys
sys.dont_write_bytecode = True
from tkinter import *
import Lister as li
import Renommage as rename
import Interface as inter

"""
Classe d'affichage de la simulation du renommage

Auteur: Antoine DEBOUT
Date de création: 28/11/2017
Version: 1.0
"""
class Simulation:
    """
    Constructeur de la classe Simulation
    @param String: message 
    @param Dict : dict
    """
    def __init__(self,m,d,nr,r,e):
        self.message = m
        self.dict = d
        self.regle = r
        self.nomRepertoire = nr
        self.extensions = e

        self.root = Tk()
        self.root.title("Simulation du renommage")
        self.fenrw = 800
        self.fenrh = 600
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        self.root.resizable(width=False, height=False)
        self.root.geometry("%dx%d+%d+%d" % (self.fenrw, self.fenrh, (self.sw-self.fenrw)/2, (self.sh-self.fenrh)/2))

        self.frameZoneTxt = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameZoneTxt.place(x=5, y=5)
        self.txtZone = Text(self.frameZoneTxt,width=92, height=30)
        self.txtZone.pack(pady=5,padx=5,side=LEFT, fill=Y)
        self.scroll = Scrollbar(self.frameZoneTxt)
        self.scroll.pack(side=RIGHT, fill=Y)
        self.scroll.config(command=self.txtZone.yview)
        self.txtZone.config(yscrollcommand=self.scroll.set)
        self.txtZone.insert(INSERT,self.message)

        self.frameBouton = Frame(self.root,borderwidth=0,relief=GROOVE)
        self.frameBouton.place(x=15,y=550)
        self.boutonNon = Button(self.frameBouton, text="Non, retourner à l'interface", command=self.onClickBtnNon)
        self.boutonNon.pack(side=LEFT,fill="both")
        self.boutonOui = Button(self.frameBouton, text="Oui, renommer", command=self.onClickBtnOui)
        self.boutonOui.pack(side=RIGHT,fill="both",padx=500)

    def onClickBtnNon(self):
        self.root.destroy()
        striExten =""
        for extension in self.extensions.split(","):
            striExten += extension.strip()[2:-1]+","
        exts = striExten[1:-2]
        if exts == "*":
            exts = ""
        i = inter.Interface(self.regle.amorce,self.regle.apartirde,self.regle.prefixe,self.regle.nomfichier,self.regle.postfixe,exts,self.nomRepertoire)
        return True

    def onClickBtnOui(self):
        renomme = rename.Renommage(self.nomRepertoire, self.regle)
        renomme.renommer(self.message, self.dict)
        self.root.destroy()
        lister = li.Lister()
        return True

