import sys
sys.dont_write_bytecode = True
from tkinter import *

"""
Classe d'affichage de la fenetre à propos

Auteur: Antoine DEBOUT
Date de création: 28/11/2017
Version: 1.0
"""
class Apropos:
    """
        Constructeur de la classe
        @param Null
    """
    def __init__(self):
        self.root = Toplevel()
        self.root.title("A propos")
        self.fenrw = 400
        self.fenrh = 400
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        self.root.geometry("%dx%d+%d+%d" % (self.fenrw, self.fenrh, (self.sw-self.fenrw)/2, (self.sh-self.fenrh)/2))
        self.photo = PhotoImage(file="../img/tyrion.png")
        self.canvas =  Canvas(self.root, width=272, height=204)   
        self.canvas.create_image(0, 0, anchor=NW, image=self.photo)
        self.canvas.pack(pady=10)
        self.label = Label(self.root, text="ELIX - Logiciel de renommage de répertoire\nDéveloppeur : ANTOINE DEBOUT\nDate de création : 24/11/2017\n\n\nVersion 1.0")
        self.label.pack()
        self.root.mainloop()

