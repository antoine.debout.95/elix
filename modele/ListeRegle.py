import sys
sys.dont_write_bytecode = True
import json
from Regle import *
import os

"""
Classe Modele des Listes de Regles de l'application

Auteur: Antoine DEBOUT
Date de création: 28/11/2017
Version: 1.0
"""
class ListeRegle:
    """
    Constructeur de la classe ListeRegle
    @param List regles
    """
    def __init__(self, regles=[]):
        self.regles = regles
    
    """
    Getter de l'attribut regles
    @return List
    """
    def _get_regles(self):
        return self.regles

    """
    Setter de l'attribut regles
    @param Regle r

    @return True 
    @return String
    """
    def _set_regles(self, r):
        try:
            self.regles = r
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : Regle Object attendu"

    """
    Methode de chargement des regles via un fichier .ini
    La structure de ce fichier est de type JSON

    @return ListeRegle()
    """
    def charger(self):
        file= open(os.path.dirname(os.path.abspath(__file__))+"\..\config\elix.ini","r")
        file_r = file.read()
        datastore = json.loads(file_r)
        file.close()
        #datastore = json.loads(open("../config/elix.ini","r").read())
        regles = datastore["regles"]
        listeRegleObj = ListeRegle()
        for i in regles:
            regle = regles[i]
            r = Regle(regle["amorce"],regle["apartirde"],regle["prefixe"],regle["nomfichier"], regle["postfixe"], regle["extensions"])
            listeRegleObj.ajouteRegle(r)
        return listeRegleObj
    
    """
    Méthode de sauvegarde des règles vers un fichier de configuration .ini (config/elix.ini)
    La structure des données est au type JSON

    @return True
    """
    def sauvegarder(self):
        f_d = open(os.path.dirname(os.path.abspath(__file__))+"\..\config\elix.ini","w")
        dic = {"regles":{}}
        dicRelge = {}
        i=0
        for regle in self.regles:
            dicRelge = {"amorce":regle.amorce,"apartirde":regle.apartirde,"prefixe":regle.prefixe,"nomfichier":regle.nomfichier, "postfixe":regle.postfixe, "extensions":regle.extensions}
            dic["regles"][i] = dicRelge
            i = i +1
        f_d.write(json.dumps(dic, indent=4, separators=(',',':')))
        f_d.close()
        return True

    """
    Méthode d'ajout d'une règle dans l'attribut regle
    @param Regle r

    @return True
    """
    def ajouteRegle(self, r):
        self.regles.append(r)
        return True

    """
    Méthode toString de la classe ListeRegle

    @return String
    """                 
    def __str__(self):
        return "Classe de liste des règles"





