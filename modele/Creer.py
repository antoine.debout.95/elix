import sys
sys.dont_write_bytecode = True
from tkinter import *
from tkinter import messagebox
from ListeRegle import *
from Regle import *
import Interface as inter

"""
Classe Interface d'affichage de la création d'une règle

Auteur: Antoine DEBOUT
Date de création : 30/11/2017
Version : 1.0
"""
class Creer:
    """
    Constructeur de la classe Creer
    @param
    """
    def __init__(self):
        """
        Création de la fenêtre
        """
        self.root = Tk()
        self.root.title("Création de règle")
        self.fenrw = 300
        self.fenrh = 450
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        self.root.resizable(width=False, height=False)
        self.root.geometry("%dx%d+%d+%d" % (self.fenrw, self.fenrh, (self.sw-self.fenrw)/2, (self.sh-self.fenrh)/2))
        
        """
        Label Principal
        """
        self.labelMain = Label(self.root,text="Création de Règle", font=("Helvetica",14))
        self.labelMain.pack()

        """
        Définition Frame Amorce
        """
        self.frameAmorce = Frame(self.root,borderwidth=0, relief=GROOVE)
        self.frameAmorce.place(x=20, y=50)
        self.labelAmorce = Label(self.frameAmorce, text="Amorce")
        self.labelAmorce.pack(side="left", fill="both")
        self.optionListeAmorce = ("Aucun","Chiffre","Lettre")
        self.stringAmort = StringVar()
        self.stringAmort.set(self.optionListeAmorce[0])
        self.listMenuAmorce = OptionMenu(self.frameAmorce, self.stringAmort, *self.optionListeAmorce)
        self.listMenuAmorce.pack(side="right", fill="both",padx=50)


        """
        Définition Frame Champ A parir de
        """
        self.frameApartirde = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameApartirde.place(x=20, y=100)
        self.labelApartirde = Label(self.frameApartirde, text="A partir de")
        self.labelApartirde.pack(side="left", fill="both")
        self.saisieApartirde = Entry(self.frameApartirde, width=20)
        self.saisieApartirde.pack(side="right",fill="both",padx=40)


        """
        Définition Frame Champ Préfix
        """
        self.framePrefixe = Frame(self.root, borderwidth=0,relief=GROOVE)
        self.framePrefixe.place(x=20, y=150)
        self.labelPrefixe = Label(self.framePrefixe, text="Préfixe")
        self.labelPrefixe.pack(side="left", fill="both")
        self.saisiePrefixe = Entry(self.framePrefixe, width=20)
        self.saisiePrefixe.pack(side="right", fill="both", padx=60)

        """
        Définition Frame Radio Bouton Nom Fichier
        """
        self.frameRadioNom = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameRadioNom.place(x=20, y=200)
        self.labelRadioNom = Label(self.frameRadioNom, text="Nom du fichier")
        self.labelRadioNom.pack(side="left", fill="both")
        self.valeurRadioNom = StringVar()
        self.valeurRadioOtherNom = StringVar()
        self.choix1 = Radiobutton(self.frameRadioNom, text="Nom Original", variable=self.valeurRadioNom, value=1)
        self.choix1.pack()
        self.choix1.select()
        self.frameRadioEntry= Frame(self.frameRadioNom, borderwidth=0, relief=GROOVE)
        self.frameRadioEntry.pack(side=LEFT, padx=5,pady=5)
        self.choix2 = Radiobutton(self.frameRadioEntry, variable=self.valeurRadioNom, value=2)
        self.entryChoix = Entry(self.frameRadioEntry, textvariable=self.valeurRadioOtherNom)
        self.choix2.pack(side=LEFT)
        self.entryChoix.pack(side=RIGHT)

        """
        Définition Frame Champ Postfixe
        """
        self.framePostfixe = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.framePostfixe.place(x=20,y=300)
        self.labelPostfixe = Label(self.framePostfixe, text="Postfixe")
        self.labelPostfixe.pack(side="left", fill="both")
        self.saisiePostfixe = Entry(self.framePostfixe, width=20)
        self.saisiePostfixe.pack(side="right", fill="both", padx=60)

        """
        Définition Frame Champs Extension
        """
        self.frameExtension = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameExtension.place(x=20, y=350)
        self.labelExtension = Label(self.frameExtension, text="Extensions")
        self.labelExtension.pack(side="left", fill="both")
        self.saisieExtension = Entry(self.frameExtension, width=20)
        self.saisieExtension.pack(side="right", fill="both",padx=60)

        """
        Définition du bouton
        """
        self.boutonCreer = Button(self.root, text="CREER", width=35,command=self.clickBouton)
        self.boutonCreer.place(x=20, y=400)

    """
    Méthode de création d'une règle puis sauvegarde dans le fichier
    lr : Appel de la classe ListeRegle()
    """
    def clickBouton(self):
        amorce = self.stringAmort.get()
        apartirde = self.saisieApartirde.get()
        prefix = self.saisiePrefixe.get()
        if len(apartirde) <= 3 and ((amorce == "Chiffre" and (apartirde.isnumeric() or apartirde =="")) or (amorce=="Lettre" and (apartirde.isalpha() or apartirde ==""))) or amorce=="Aucun":
            if self.valeurRadioNom.get() == "1":
                nomFichier = True
            if self.valeurRadioNom.get() == "2":
                nomFichier = self.valeurRadioOtherNom.get()
            postfixe = self.saisiePostfixe.get()
            extensions = self.saisieExtension.get()
            stri = "Amorce: "+amorce+"\nA partir de : "+apartirde+"\nPrefix: "+prefix+"\nNom Fichier : "+str(nomFichier)+"\nPostfixe: "+postfixe+"\nExtensions: "+extensions
            question = messagebox.askyesno("Création de la règle", "Etes-vous sur de vouloir créer la règle avec les attributs suivants : \n"+stri, icon="question")
            if question == True:
                lr = ListeRegle()
                lr.regles[:] = []
                lr.charger()
                r = Regle(amorce,apartirde,prefix, nomFichier,postfixe)
                for ext in extensions.split(','):
                    r.ajoutExtension("*"+ext)
                lr.ajouteRegle(r)
                lr.sauvegarder()
                r.extensions[:] = []
                messagebox.showinfo("Création de la règle","Règle créée avec succès !")
                self.root.destroy()
                inter.Interface(amorce, apartirde, prefix,nomFichier,postfixe,extensions)
            else:
                return
        else:
            messagebox.showerror("Erreur", "L'amorce et A partir de ne sont pas cohérents.")
            return
