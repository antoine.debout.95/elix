import sys
sys.dont_write_bytecode = True
from tkinter import *
from tkinter import messagebox
from ListeRegle import *
from Regle import *
import Interface as inter

"""
Classe Interface d'affichage d'une liste des règles créées

Auteur: Antoine DEBOUT
Date de création: 07/12/2017
Version: 1.0
"""
class Lister:
    """
    Constructeur de la classe Lister
    @param
    """
    def __init__(self):
        """
        Initialisation des règles à afficher
        """
        self.lr = ListeRegle()
        self.lr.regles[:] = []
        self.lr.charger()
        
        """
        Création de la fenêtre
        """
        self.root = Tk()
        self.root.title("Liste des règles")
        self.fenrw = 550
        self.fenrh = 300
        self.root.resizable(width=False, height=False)
        self.sw = self.root.winfo_screenwidth()
        self.sh = self.root.winfo_screenheight()
        self.root.geometry("%dx%d+%d+%d" % (self.fenrw, self.fenrh, (self.sw-self.fenrw)/2, (self.sh-self.fenrh)/2))

        """
        Label titre principal
        """
        self.labelMain = Label(self.root,text="Liste des Règles", font=("Helvetica",14))
        self.labelMain.pack()

        """
        Création de la ListeBox de toutes les listess
        """
        self.frameListe = Frame(self.root, borderwidth=0, relief=GROOVE)
        self.frameListe.place(x=20, y=50)
        self.scrollY = Scrollbar(self.frameListe)
        self.scrollY.pack(side=RIGHT, fill=Y)
        self.listeBoxRegle = Listbox(self.frameListe, width=85)
        self.listeBoxRegle.pack()
        self.scrollY.config(command=self.listeBoxRegle.yview)
        self.listeBoxRegle.insert(0, "Amorce      |      A partir de      |      Préfixe      |      Nom Fichier      |      Postfixe      |      Extensions")
        self.disable_item("0")

        """
        Création bouton Utiliser
        """
        self.boutonUtiliser = Button(self.root, width=73, text="Utiliser", command=self.utiliser)
        self.boutonUtiliser.place(x=20, y=250)

        """
        Ajout des règles
        """
        self.ajouterRegles()

    """
    Méthode d'insertion des règles dans la liste box

    @return True
    """
    def ajouterRegles(self):
        i= 1
        for regle in self.lr.regles:
            self.listeBoxRegle.insert(i, regle.amorce+"             |     "+regle.apartirde+"     |     "+regle.prefixe+"     |     "+str(regle.nomfichier)+"     |     "+regle.postfixe+"     |     "+str(regle.extensions))
            i+= 1
        return True
    """
    Méthode de désactivation d'une ligne (utilisé pour définir l'en tête de la liste box)
    @param int : index
    """
    def disable_item(self,index):
        self.listeBoxRegle.itemconfig(index, fg="gray")
        self.listeBoxRegle.bind("<<ListboxSelect>>", lambda event, index=index: self.no_selection(event,index))
    
    """
    Méthode de désactivation d'une ligne (utilisé pour définir l'en tête de la liste box)
    @param lamba : event
    @param int : index
    """
    def no_selection(self,event,index):
        if str(self.listeBoxRegle.curselection()[0]) in str(index):
            self.listeBoxRegle.selection_clear(index)

    """
    Méthode appelée lors du clic sur le bouton
    Changement d'interface
    r: Règle

    @return Bool
    """
    def utiliser(self):
        if self.listeBoxRegle.curselection():
            index = self.listeBoxRegle.curselection()[0]
            rstring = self.listeBoxRegle.get(index)
            rlist = rstring.split("|")
            amorce = rlist[0].strip()
            apartirde = rlist[1].strip()
            prefixe = rlist[2].strip()
            nomfichier = rlist[3].strip()
            if nomfichier == "True":
                nomfichier = True
            postfixe = rlist[4].strip()
            extensions = rlist[5].strip()[1:-1]
            striExten =""
            for extension in extensions.split(","):
                striExten += extension.strip()[2:-1]+","
            exts = striExten[:-1]
            if exts == "*":
                exts = ""
            self.root.destroy()
            main = inter.Interface(amorce,apartirde,prefixe,nomfichier,postfixe,exts)
            return True
        else:
            messagebox.showerror("Erreur", "Veuillez sélectionner un élément")
            return False
        
