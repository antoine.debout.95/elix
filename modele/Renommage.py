import sys
sys.dont_write_bytecode = True
from Action import *
from Regle import *
from tkinter import messagebox
import os

"""
Classe Modele de Renommage
Hérité de la classe Action

Auteur: Antoine DEBOUT
Date de création: 28/11/2017
Version: 1.0
"""
class Renommage(Action):
    """
    Constructeur de la class renommage
    @param String n
    @param Regle r
    """
    def __init__(self,n,r):
        Action.__init__(self,n,r)
    
    """
    Methode pour renommer un fichier
    @return Bool
    """
    def renommer(self,message,dict):
        try:
            for original, modif in dict.items():
                os.rename(original,modif)
            messagebox.showinfo("Validation","Vos fichiers ont été renommés avec succès !")
            return True
        except OSError:
            messagebox.showerror("Erreur","Il y a eu une erreur lors du renommage !")
            return False
            
    """
    Methode toString pour la classe de renommage

    @return String
    """
    def __str__(self):
        return "Classe de renommage du fichier"


