import sys
sys.dont_write_bytecode = True
import os
import tkinter
from tkinter import messagebox
from glob import glob
from os.path import join


"""
Class Modèle pour les Actions de l'application

Auteur: Antoine DEBOUT
Date de création: 28/11/2017
Version: 1.0
"""
class Action:
    """
    Constructeur de la classe Action
    @param String nomRep : Nom du répertoire
    @param Regle regle : Règle
    """
    def __init__(self,nomRep,regle):
        self.nomRepertoire = nomRep
        self.regle = regle

    """
    Getter pour l'attribut nomRepertoire
    @return String
    """
    def _get_nomRepertoire(self):
        return self.nomRepertoire
    
    """
    Setter pour l'attribut nomRepertoire
    @param String n : Nom répertoire
    """
    def _set_nomRepertoire(self,n):
        try:
            self.nomRepertoire = n
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : String attendu"


    """
    Getter pour l'attribut regle
    @return Regle
    """
    def _get_regle(self):
        return self.regle
    
    """
    Setter pour l'attribut regle
    @param Regle r : Règle

    @return Bool True
    """
    def _set_regle(self,r):
        try:
            self.regle = r
            return True
        except ValueError:
            return "Erreur : Type de valeur non correct : Regle Object attendu"
    
    """
    Methode toString de la classe Action

    @return String
    """
    def __str__(self):
        return "Le nom du répertoire est : "+self.nomRepertoire+" et la regle est : "+str(self.regle)

    
    """
    Methode Simule() 
    Permet de simuler le renommage d'un fichier
    
    @return Tuple(String,Dict)
    """
    def simule(self):
        originalNom = ""
        extension = ""
        changer = ""
        pre = ""
        post = ""
        nomF = ""
        stri = ""
        fichiers = []
        """dic = {nomOrigine:nouveauNom}"""
        dictRetour = {} 
        try:
            if self.regle.extensions != [""]:
                for ext in self.regle.extensions:
                    fichiers.extend(glob(join(self.nomRepertoire, ext)))
                #Remise à zéro de la liste des extensions pour éviter les doublons
                self.regle.extensions[:] = []
            else:
                fichiers[:] = []
                fichiers = os.listdir(self.nomRepertoire)
            i=1
            if fichiers != "":
                nextAmorce = ""
                for fichier in fichiers:
                    if os.path.isfile(fichier):
                        originalNom = os.path.basename(fichier).split(".")[0]
                        repertoire = os.path.dirname(fichier)
                        extension = os.path.splitext(fichier)[1]
                        stri += " - Nom du fichier original : "+ originalNom+extension+" \n - Nouveau Nom : "
                        
                        if nextAmorce == "":
                            amorce = self.regle.apartirde
                            nextAmorce = amorce
                        else:
                            amorce = self.getAmorce(nextAmorce, self.regle.amorce)
                            nextAmorce = amorce
                        stri+=amorce
                        if self.regle.prefixe != "":
                            stri += self.regle.prefixe
                            pre = self.regle.prefixe
                            
                        if self.regle.nomfichier != True:
                            stri += self.regle.nomfichier+"_"+ str(i)
                            nomF = self.regle.nomfichier+"_"+ str(i)
                            i+=1
                        else:
                            stri += originalNom
                            nomF = originalNom                    

                        if self.regle.postfixe != "":
                            stri += self.regle.postfixe + extension+"\n"
                            post = self.regle.postfixe
                            stri +="_______________________________________________________________________________\n"
                        else:
                            stri += extension+"\n"
                            stri +="______________________________________________________________________________\n"
                        ancienNom = repertoire+"/"+originalNom+extension
                        nouveauNom = repertoire+"/"+amorce+pre+nomF+post+extension
                        dictRetour[ancienNom] = nouveauNom 

                if stri == "":
                    stri = "Erreur, une ou plusieurs extensions saisies ne sont pas dans le répertoire"
                return (stri,dictRetour)
            else:
                showinfo("Erreur""Erreur : Pas de fichiers dans ce répertoire")
        except IOError:
            return "Erreur : dossier non trouvable"

    """
    Méthode de création d'une amorce Chiffre
    @param apartirde : string 
    @return string : amorce
    """
    def getAmorce(self, apartirde,typeA):
        apartirde = apartirde.upper()
        #Si on passe de 999 à A il faut changer le type de l'amorce de chiffre a lettre
        if apartirde == "A":
            typeA = "Lettre"
        #Si on passe de ZZZ à 001 il faut changer le type de l'amorce de lettre à chiffre
        if apartirde == "001":
            typeA = "Chiffre"
        """
        Si le type de l'amorce est Aucun : il n'y a pas d'amorce
        """
        if typeA == "Aucun":
            amorce=""

        """
        Si le type de l'amorce est Chiffre: 
            - Si a partir de ou l'amorce du fichier précédent est égal à 999 : on passe l'amorce de 999 à A
            - Sinon l'amorce est égal à l'ancienne amorce + 1 (ex : 023 -> 024)
        """
        if typeA == "Chiffre":
            if int(apartirde) == 999:
                amorce = "A"
            else:
                amorce = '{:03}'.format((int(apartirde)+1))

        """
        Si le type de l'amorce est Lettre:
            - Pour le nombre de caractere dans apartirde : 1, 2 ou 3 comme on ne dépasse pas ZZZ:
                Si l'index est égale à 0, on lui retire 1 pour ne pas créer une erreur
                Sinon la lettre est égale à la lettre de l'index -i dans amorce (négatif pour aller de droite à gauche)
                On ajoute un a la lettre et on l'inclue dans une amorce fictive
                Puis on recréer l'amorce avec l'amorce fictive.
            - Si l'amorce est égale à ZZZ: on passe l'amorce de ZZZ à 001
        """
        if typeA == "Lettre":
            amorce = apartirde
                                
            for i in range(len(apartirde)):
                if i == 0:
                    lettre = amorce[-i-1]
                else:
                    lettre = amorce[-i]
                if amorce != "ZZZ":
                    if lettre != "Z":
                        lettre = chr(int(ord(lettre)) + 1)
                        amorceList = list(amorce)

                        if i == 0:
                            amorceList[-i-1] = lettre
                        else:
                            amorceList[-i] = lettre
                        amorce = ""
                        for a in amorceList:
                            amorce+=a

                        break
                    else:
                        if len(amorce) == 3:
                            if amorce[2] == "Z":
                                if amorce[1] == "Z":
                                    amorce = chr(int(ord(amorce[0])+1)) + "AA"
                                else:
                                    amorce = amorce[0] + chr(int(ord(amorce[1])+1)) + "A"
                            else:
                                amorce = amorce[0:1] + chr(int(ord(amorce[2]) + 1)) + "A"
                        elif len(amorce) == 2:
                            if amorce[1] == "Z":
                                if amorce == "ZZ":
                                    amorce = "AAA"
                                else:
                                    amorce = chr(int(ord(amorce[0])+1)) + "A"
                            else:
                                amorce = amorce[0] + chr(int(ord(amorce[1])+1))
                        else:
                            if amorce == "Z":
                                amorce = "AA"
                            else:
                                amorce = chr(int(ord(amorce)+1))

                        break
                else:
                    amorce = "001"
                    break
        

        return amorce