import unittest
import sys
import os
#Blocage du pycache
sys.dont_write_bytecode = True
sys.path.append(sys.path.append(os.path.dirname(os.path.abspath(__file__))+"\..\modele"))
from Action import *
from Regle import *

"""
Classe de Test Unitaire pour la classe Action

Auteur: Antoine DEBOUT
Date de création: 09/12/2017
Version: 1.0
"""
class TestLister(unittest.TestCase):
    """
    Methode de test de la fonction
    Action::_get_nomRepertoire()
    """
    def test_get_nomRepertoire(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = Action("D:\workspace\python\elix\test/test", r1)
        get = a._get_nomRepertoire()
        self.assertEqual(get,"D:\workspace\python\elix\test/test")
    """
    Methode de test de la fonction
    Action;;_set_nomRepertoire()
    """
    def test_set_nomRepertoire(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = Action("D:\workspace\python\elix\test/test", r1)
        a.nomRepertoire = "Toto"
        get = a._get_nomRepertoire()
        self.assertTrue(a._set_nomRepertoire("Toto"))
        self.assertEqual(get,"Toto")
    """
    Methode de test de la fonction
    Action::_get_regle()
    """
    def test_get_regle(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = Action("D:\workspace\python\elix\test/test", r1)
        get = a._get_regle()
        self.assertIsInstance(get,Regle)
    """
    Methode de test de la fonction
    Action::_set_regle()
    """    
    def test_set_regle(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = Action("D:\workspace\python\elix\test/test", r1)
        r2 = Regle("Chiffre","002","preFixe",True,"Postfixe")
        a.regle = r2
        get = a._get_regle()
        self.assertTrue(a._set_regle(r2))
        self.assertEqual(get,r2)
    
    """
    Methode de test de la fonction
    Action::getAmorce()
    """
    def test_getAmorce(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = Action("D:\workspace\python\elix\test/test", r1)
        amorceC = a.getAmorce("B","Lettre")
        amorceC2 = a.getAmorce("BC","Lettre")
        amorceC3 = a.getAmorce("EFG","Lettre")
        amorceL = a.getAmorce("2","Chiffre")
        amorceL2 = a.getAmorce("23","Chiffre")
        amorceL3 = a.getAmorce("425","Chiffre")
        self.assertEqual(amorceC,"C")
        self.assertEqual(amorceC2,"BD")
        self.assertEqual(amorceC3,"EFH")
        self.assertEqual(amorceL,"003")
        self.assertEqual(amorceL2,"024")
        self.assertEqual(amorceL3,"426")

if __name__ == '__main__':
    unittest.main()