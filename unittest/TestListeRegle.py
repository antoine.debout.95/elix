import unittest
import sys
import os
#Blocage du pycache
sys.dont_write_bytecode = True
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"\..\modele")
from ListeRegle import *
from Regle import *

"""
Classe de Test Unitaire pour la classe Action

Auteur: Antoine DEBOUT
Date de création: 09/12/2017
Version: 1.0
"""
class TestListeRegle(unittest.TestCase):
    """
    Methode de test de la fonction
    ListeRegle::_get_regles()
    """
    def test_get_regles(self):
        l = ListeRegle()
        l.regles = ["toto","tata"]
        get = l.regles
        self.assertEqual(get,["toto","tata"])

    """
    Methode de test de la fonction
    ListeRegle::_set_regles()
    """
    def test_set_regles(self):
        l = ListeRegle()
        l.regles = ["toto","tata"]
        get = l.regles
        self.assertEqual(get,["toto","tata"])
        self.assertTrue(l._set_regles(["toto"]))

    """
    Methode de test de la fonction
    ListeRegle::charger()
    """
    def test_charger(self):
        l = ListeRegle()
        self.assertIsInstance(l.charger(), ListeRegle)
    
    """
    Methode de test de la fonction
    ListeRegle::sauvegarder()
    """
    def test_sauvegarder(self):
        l = ListeRegle()
        l.charger()
        self.assertTrue(l.sauvegarder(), ListeRegle)
    
    """
    Methode de test de la fonction
    ListeRegle::ajouteRegle()
    """
    def test_ajouteRegle(self):
        l = ListeRegle()
        r1 = Regle("Lettre","A","pré",True,"post")
        test = l.ajouteRegle(r1)
        self.assertTrue(r1)
        self.assertIsInstance(l.regles[0], Regle)
        self.assertEqual(r1, l.regles[0])

if __name__ == '__main__':
    unittest.main()