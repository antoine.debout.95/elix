import unittest
import sys
import os
#Blocage du pycache
sys.dont_write_bytecode = True
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"\..\modele")
from Lister import *

"""
Classe de Test Unitaire pour la classe d'interface Lister

Auteur: Antoine DEBOUT
Date de création: 09/12/2017
Version: 1.0
"""
class TestLister(unittest.TestCase):
    """
    Methode de test de la fonction
    Lister::utiliser()
    """
    def test_utiliser(self):
        l =Lister()
        l.utiliser()
        self.assertTrue(l)
        
    """
    Methode de test de la fonction
    Lister::ajouterRegles()
    """
    def test_ajouterRegles(self):
        l =Lister()
        k = l.ajouterRegles()
        self.assertTrue(k)
    
if __name__ == '__main__':
    unittest.main()