import unittest
import sys
import os
sys.dont_write_bytecode = True
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"\..\modele")
from Regle import *

"""
Classe de Test Unitaire pour la classe Regle

Auteur: Antoine DEBOUT
Date de création: 09/12/2017
Version: 1.0
"""
class TestRegle(unittest.TestCase):
    """
    Methode de test de la fonction
    Regle::_get_amorce()
    """
    def test_get_amorce(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = r1._get_amorce()
        self.assertEqual(a, "Lettre")
    
    """
    Methode de test de la fonction
    Regle::_set_amorce()
    """
    def test_set_amorce(self):
      r1 = Regle("Lettre","A","pré",True,"post")
      self.assertTrue(r1._set_amorce("Chiffre"))
      self.assertEqual(r1.amorce, "Chiffre")  

    """
    Methode de test de la fonction
    Regle::_get_apartirde()
    """
    def test_get_apartirde(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        a = r1._get_apartirde()
        self.assertEqual(a, "A")

    """
    Methode de test de la fonction
    Regle::_set_apartirde()
    """
    def test_set_apartirde(self):
      r1 = Regle("Lettre","A","pré",True,"post")
      self.assertTrue(r1._set_apartirde("V"))
      self.assertEqual(r1.apartirde, "V")


    """
    Methode de test de la fonction
    Regle::_get_prefix()
    """
    def test_get_prefix(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        p = r1._get_prefix()
        self.assertEqual(p, "pré")

    """
    Methode de test de la fonction
    Regle::_set_prefix()
    """
    def test_set_prefix(self):
      r1 = Regle("Lettre","A","pré",True,"post")
      self.assertTrue(r1._set_prefix("prefixe"))
      self.assertEqual(r1.prefixe, "prefixe")
    

    """
    Methode de test de la fonction
    Regle::_get_nomFichier()
    """
    def test_get_nomFichier(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        n = r1._get_nomFichier()
        self.assertEqual(n, True)

    """
    Methode de test de la fonction
    Regle::_set_nomFichier()
    """
    def test_set_nomFichier(self):
      r1 = Regle("Lettre","A","pré",True,"post")
      self.assertTrue(r1._set_nomFichier("Toto"))
      self.assertEqual(r1.nomfichier, "Toto")

    """
    Methode de test de la fonction
    Regle::_get_postfixe()
    """
    def test_get_postfixe(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        p = r1._get_postfixe()
        self.assertEqual(p, "post")

    """
    Methode de test de la fonction
    Regle::_set_postfixe()
    """
    def test_set_postfixe(self):
      r1 = Regle("Lettre","A","pré",True,"post")
      self.assertTrue(r1._set_postfixe("Postfixe"))
      self.assertEqual(r1.postfixe, "Postfixe")

    """
    Methode de test de la fonction
    Regle::_get_extensions()
    """
    def test_get_extensions(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        r1.ajoutExtension("*.txt")
        p = r1._get_extensions()
        self.assertEqual(p[0], "*.txt")

    """
    Methode de test de la fonction
    Regle::_set_extensions()
    """
    def test_set_extensions(self):
      r1 = Regle("Lettre","A","pré",True,"post")
      self.assertTrue(r1._set_postfixe(["*.txt"]))
      self.assertEqual(r1.extensions, ["*.txt"])

    """
    Methode de test de la fonction
    Regle::ajoutExtension()
    """
    def test_ajoutExtension(self):
        r1 = Regle("Lettre","A","pré",True,"post")
        self.assertTrue(r1.ajoutExtension("*.txt"))
        self.assertEqual(r1.extensions, ["*.txt"])
        r1.extensions[:] = []
if __name__ == '__main__':
    unittest.main()