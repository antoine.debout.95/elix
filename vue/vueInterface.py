"""
Vue de l'interface Main

Auteur: DEBOUT Antoine
Date de creation: 30/11/2017
Version: 1.0
"""
import sys
import os
#Blocage du pycache
sys.dont_write_bytecode = True
sys.path.append(sys.path.append(os.path.dirname(os.path.abspath(__file__))+"\..\modele"))

from Interface import *
from Action import *
from Lister import *


home = Interface()
